package main

import (
	"fmt"
	"net/http"
    "time"
    "os"
	"strings"
    "bufio"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func write(text string, file *os.File) {
    if _, err := file.WriteString(text); err != nil {
        panic(err)
    }
}

func read(filename string) []string {
	file, err := os.Open(filename)
    check(err)
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
    var data []string
  
    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

	return data
}

func showTime(w http.ResponseWriter, req *http.Request) {
    currentTime := time.Now()

	if req.Method == http.MethodGet {
		fmt.Fprintf(w, currentTime.Format("15h04"))
	}
}

func addPayload(file *os.File) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		if req.Method == http.MethodPost {
			if err := req.ParseForm(); err != nil {
				fmt.Println("Une erreur est survenue")
				fmt.Fprintln(w, "Une erreur est survenue")
				return
			}
			for key, value := range req.PostForm {
				write(key+": "+strings.Join(value, ";")+"\n", file)
			}
			fmt.Fprintf(w, "Informations reçues: %v\n", req.PostForm)
		}
	}
}

func getEntries(filename string) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		if req.Method == http.MethodGet {
			data := read(filename)
			var entries []string

			for _, line := range data {
				entry := strings.Split(line, ": ")
				if entry[0] == "entry" {
					entries = append(entries, entry[1])
				}
			}

			fmt.Fprintf(w, strings.Join(entries, "\n"))
		}
	}
}

func main() {
	filename := "serverDb.txt"
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
    defer file.Close()
    check(err)

	http.HandleFunc("/", showTime)
	http.HandleFunc("/add", addPayload(file))
	http.HandleFunc("/entries", getEntries(filename))
	http.ListenAndServe(":4567", nil)
}